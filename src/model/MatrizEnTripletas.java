package model;

public class MatrizEnTripletas {

    private Tripleta[] vectorTripletas;

    public MatrizEnTripletas(Tripleta tripleta) {
        int m = tripleta.getFila();
        int n = tripleta.getColumna();
        int p = m * n + 2;

        vectorTripletas = new Tripleta[p];
        vectorTripletas[0] = tripleta;

        for (int i = 1; i < p; i++) {
            vectorTripletas[i] = null;
        }

    }

    public void asignarTripleta(Tripleta tripleta, int posicion) {
        vectorTripletas[posicion] = tripleta;
    }

    public void asignarNumeroTripletas(int n) {
        Tripleta tripleta = vectorTripletas[0];
        tripleta.setValor(n);
        vectorTripletas[0] = tripleta;
    }

    public int getNumeroFilas() {
        return vectorTripletas[0].getFila();
    }

    public int getNumeroColumnas() {
        return vectorTripletas[0].getColumna();
    }

    public int getNumeroTripletas() {
        return vectorTripletas[0].getValor();
    }

    public Tripleta getTripleta(int i) {
        return vectorTripletas[i];
    }

    public void mostrarMatrizEnTripletas() {
        int i = 1;
        while (vectorTripletas[i] != null && i <= vectorTripletas[0].getValor()) {
            System.out.println(vectorTripletas[i].getFila() + "," + vectorTripletas[i].getColumna() + "," + vectorTripletas[i].getValor());
            i++;
        }
    }

    public void insertarTripleta(Tripleta tripleta) {
        int numTripletas = vectorTripletas[0].getValor();
        int i = 1;
        int j;
        Tripleta tripleta1 = getTripleta(i);

        while (i <= numTripletas && tripleta1.getFila() < tripleta.getFila()) {
            i++;
            tripleta1 = getTripleta(i);
        }

        while (i <= numTripletas && tripleta1.getFila() == tripleta.getFila()
                && tripleta1.getColumna() <= tripleta.getColumna()) {
            i++;
            tripleta1 = getTripleta(i);
        }

        numTripletas++;
        j = numTripletas - 1;

        while (j >= i){
            vectorTripletas[j+1] = vectorTripletas[j];
            j--;
        }
        vectorTripletas[i] = tripleta;
        asignarNumeroTripletas(numTripletas);
    }

}
